# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.sort.last - arr.sort.first
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  if arr.sort == arr
    return true
  end
  false
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  vowels = ['a','e','i','o','u']
  counter = 0
  vowels.each do |char|
    if str.downcase.include? char
      counter+=1
    end
  end
  counter
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  vowels = ['a','e','i','o','u']
  newstring=''
  str.downcase.each_char do |char|
    if vowels.include? char
      newstring
    else
      newstring+=char
    end
  end
  newstring
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  length=str.length
  i=0
  while (i+1) < length
    if str[i].downcase == str[i+1].downcase
      return true
    end
    i+=1
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  arr=arr.join
  newarray= "(" + arr[0..2] + ") " + arr[3..5] + '-' + arr[6..9]
  # your code goes here
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  str.split(",").sort.last.to_i - str.split(",").sort.first.to_i
  # your code goes here
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  if offset < 0
    offset = (arr.length - offset.abs)
  elsif offset > arr.length
    offset = offset % arr.length
  end
  rotate = arr.drop(offset)
  rotate.concat(arr.take(offset))
  rotate
end
